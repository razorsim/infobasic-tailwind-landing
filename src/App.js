function App() {
  return (
    <div className="bg-gradient-to-br from-background-900 to-background-800 min-h-screen">
      <img src="/images/advanced-frame.png" alt="logo" />
    </div>
  );
}
export default App;